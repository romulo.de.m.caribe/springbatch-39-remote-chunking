package com.emrah.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.emrah.model.Employer;

public class EmployerRowMapper implements RowMapper<Employer>{

	@Override
	public Employer mapRow(ResultSet rs, int i) throws SQLException {
		return new Employer(rs.getLong("id"), 
							rs.getString("firstName"), 
							rs.getString("lastName"), 
							rs.getString("birthDate"));
	}

}
