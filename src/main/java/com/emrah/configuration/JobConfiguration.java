package com.emrah.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.emrah.mapper.CustomerRowMapper;
import com.emrah.model.Customer;
import com.emrah.model.Employer;

@Configuration
public class JobConfiguration {
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private JobExplorer jobExplorer;
	
	@Bean
	public JdbcPagingItemReader<Customer> pagingItemReader() {
		
		JdbcPagingItemReader<Customer> reader = new JdbcPagingItemReader<Customer>();
		
		reader.setDataSource(this.dataSource);
		reader.setFetchSize(2);
		reader.setRowMapper(new CustomerRowMapper());
		
		MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();
		queryProvider.setSelectClause("id, firstName, lastName, birthDate");
		queryProvider.setFromClause("from customer");
		
		Map<String, Order> sortKeys = new HashMap<String, Order>();
		
		sortKeys.put("id", Order.ASCENDING);
		
		queryProvider.setSortKeys(sortKeys);
		
		reader.setQueryProvider(queryProvider);
		
		return reader;
	}
	
	@Bean
	public ItemProcessor<Customer, Employer> upperCaseItemProcessor() {
		return item -> new Employer(item.getId(), 
				item.getFirstName().toUpperCase(), 
				item.getLastName().toUpperCase(), 
				item.getBirthDate());
	}
	
	@Bean
	public JdbcBatchItemWriter<Employer> itemWriter() {
		JdbcBatchItemWriter<Employer> itemWriter = new JdbcBatchItemWriter<Employer>();
		
		itemWriter.setDataSource(this.dataSource);
		itemWriter.setSql("INSERT INTO EMPLOYER VALUES(:id, :firstName, :lastName, :birthDate)");
		itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Employer>());
		itemWriter.afterPropertiesSet();
		
		return itemWriter;
	}
	
	@Bean
	public TaskletStep step1() {
		return this.stepBuilderFactory.get("step1")
						.<Customer, Employer>chunk(2)
						.reader(pagingItemReader())
						.processor(upperCaseItemProcessor())
						.writer(itemWriter())
						.build();
	}
	
	@Bean
	@Profile("master")
	public Job job39() {
		return jobBuilderFactory.get("job39")
					.start(step1())
					.build();
	}
	

}
